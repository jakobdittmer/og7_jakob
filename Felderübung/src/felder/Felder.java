package felder;

import java.util.Arrays;

/**
 *
 * �bungsklasse zu Feldern
 *
 * @version 1.0 vom 05.05.2011
 * @author Tenbusch
 */

public class Felder {

	// unsere Zahlenliste zum Ausprobieren
	private int[] zahlenliste = { 5, 8, 4, 3, 9, 1, 2, 7, 6, 0 };

	// Konstruktor
	public Felder() {
	}

	// Methode die Sie implementieren sollen
	// ersetzen Sie den Befehl return 0 durch return ihre_Variable

	// die Methode soll die gr��te Zahl der Liste zur�ckgeben
	public int maxElement() {
		int max = 0;
		for (int i = 0; zahlenliste.length > i; i++) {
			if (zahlenliste[i] > max) {
				max = zahlenliste[i];
			}
		}
		return max;
	}

	// die Methode soll die kleinste Zahl der Liste zur�ckgeben
	public int minElement() {
		int min = 0;
		for (int i = 0; zahlenliste.length > i; i++) {
			if (zahlenliste[i] < min) {
				min = zahlenliste[i];
			}
		}
		return min;
	}

	// die Methode soll den abgerundeten Durchschnitt aller Zahlen zur�ckgeben
	public int durchschnitt() {
		int ergebniss = 0;
		for (int i = 0; zahlenliste.length > i; i++) {
			ergebniss = zahlenliste[i] + ergebniss;
		}
		ergebniss = ergebniss / zahlenliste.length;
		return ergebniss;
	}

	// die Methode soll die Anzahl der Elemente zur�ckgeben
	// der Befehl zahlenliste.length; k�nnte hierbei hilfreich sein
	public int anzahlElemente() {
		return zahlenliste.length;
	}

	// die Methode soll die Liste ausgeben
	public String toString() {
		return Arrays.toString(zahlenliste);
	}

	// die Methode soll einen booleschen Wert zur�ckgeben, ob der Parameter in
	// dem Feld vorhanden ist
	public boolean istElement(int zahl) {
		for (int i = 0; zahlenliste.length > i; i++) {
			if (zahlenliste[i] == zahl) {
				return true;
			}
		}
		return false;
	}

	// die Methode soll das erste Vorkommen der
	// als Parameter �bergebenen Zahl liefern oder -1 bei nicht vorhanden
	public int getErstePosition(int zahl) {
		for (int i = 0; zahlenliste.length > i; i++) {
			if (zahlenliste[i] == zahl) {
				return i;
			}
		}
		return -1;
	}

	// die Methode soll die Liste aufsteigend sortieren
	// googlen sie mal nach Array.sort() ;)
	public int[] sortiere() {
		int[] zwischenzahl = new int[zahlenliste.length];
		zwischenzahl = zahlenliste;
		Arrays.sort(zwischenzahl);
		return zwischenzahl;
	}

	public static void main(String[] args) {
		Felder testenMeinerLoesung = new Felder();
		System.out.println(testenMeinerLoesung.maxElement());
		System.out.println(testenMeinerLoesung.minElement());
		System.out.println(testenMeinerLoesung.durchschnitt());
		System.out.println(testenMeinerLoesung.anzahlElemente());
		System.out.println(testenMeinerLoesung.toString());
		System.out.println(testenMeinerLoesung.istElement(9));
		System.out.println(testenMeinerLoesung.getErstePosition(5));
		testenMeinerLoesung.sortiere();
		System.out.println(testenMeinerLoesung.toString());
		//g
	}
}
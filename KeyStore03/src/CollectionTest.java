import java.util.*;

public class CollectionTest {

	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Buch> buchliste = new LinkedList<Buch>();

		Buch buch1 = new Buch("LoL", "Riot", "ISBN-10:2");
		Buch buch2 = new Buch("Ich", "Vorname", "ISBN-10:1");
		buchliste.add(buch1);
		buchliste.add(buch2);

		char wahl;
		@SuppressWarnings("unused")
		String eintrag;

		@SuppressWarnings("unused")
		int index;
		do {
			menue();
			wahl = Tastatur.liesChar();
			switch (wahl) {
			case '1':
				buchliste.add(nochnenbuch());
				break;
			case '2':
				System.out.println("Bitte ISBN-Nummer des zu suchenden Buches Angeben:");
				Buch temp = CollectionTest.findeBuch(buchliste, Tastatur.liesString());
				if (temp != null) {
					temp.toString();
				}
				break;
			case '3':
				System.out.println("Index des zu l�schenden Buches angeben:"); // Ich hatte leider keine Methode womit
																				// der Benutzter eine Objektreferenz
																				// eingibt ._.
				CollectionTest.deleteBuch(buchliste, buchliste.get(Tastatur.liesInt() - 1));
				break;
			case '4':
				System.out.print(CollectionTest.gr��teISBN(buchliste));
				break;
			case '5':
				buchliste.get(Tastatur.liesInt() - 1).toString();
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = Tastatur.liesChar();
			} // switch

		} while (true);
	}// main

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		for (int i = 0; i < buchliste.size(); i++) {
			if (buchliste.get(i).getIsbn().equals("ISBN-10:" + isbn)) {
				return buchliste.get(i);
			}
		}
		return null;
	}

	public static Buch nochnenbuch() {
		System.out.println("Titel eingeben...");
		String titel = Tastatur.liesString();
		System.out.println("Autor angeben...");
		String autor = Tastatur.liesString();
		System.out.print("ISBN Nummer eingeben...");
		String isbn = "ISBN-10:" + Tastatur.liesString();
		return new Buch(titel, autor, isbn);
	}
//lol
	
	public static boolean deleteBuch(List<Buch> buchliste, Buch buch) {
		return buchliste.remove(buch);
	}

	public static String gr��teISBN(List<Buch> buchliste) {
		Buch temp = buchliste.get(0);
		for (int i = 0; i < buchliste.size(); i++) {
			if (temp.compareTo(buchliste.get(i)) < 0) {
				temp = buchliste.get(i);
			}
		}
		return temp.getIsbn();
	}
}

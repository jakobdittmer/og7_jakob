package pack1;

public class Primzahlfinder {

	private static long[] zeitsammlung = new long[10];
	private static long n;
	private static long t;

	public static boolean primzahlCheck(long n) {
		for (long i = 2; i < n; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	public static double druchschnitt(long[] zahlen) {
		double sum = 0;
		for (int i = 0; i < zahlen.length; i++)
			sum = sum + zahlen[i];
		return (sum / zahlen.length);
	}

	public static void main(String[] args) {
		Stopuhr s1 = new Stopuhr();

		System.out.println("Laufzeitexperiment! \nPrimzahl eingeben...");
		n = Tastatur.liesLong();

		for (int i = 0; i < 10; i++) {
			s1.start();
			System.out.println(primzahlCheck(n));
			s1.stop();
			System.out.println("t in millsec: " + s1.getZeit());
			zeitsammlung[i] = s1.getZeit();
			s1.reset();

		}
		System.out.println("beendet!");

		t = Math.round(druchschnitt(zeitsammlung));
		System.out.println(t);

	}

}

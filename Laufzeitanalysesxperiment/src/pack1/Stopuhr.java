package pack1;

public class Stopuhr {
	
	private long stop;
	private long start;
	
	public void start() {
		this.start = System.currentTimeMillis();
	}
	
	public void stop() {
		this.stop = System.currentTimeMillis();
	}
	
	public void reset() {
		this.start = 0;
		this.stop = 0;
	}
	
	public long getZeit() {
		return (stop-start);
	}

	public Stopuhr() {
		super();
	}

}

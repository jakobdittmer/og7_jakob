package pack1;

//package prjdbtest;

import java.io.*;

/**
 *
 * Die Klasse stellt Operationen zum Einlesen von Werten der Datentypen boolean,
 * char, int, long, float, double und String mit der Tastatur zur
 * Verf�gung.<br />
 * Fehlerhafte Eingaben werden standardm��ig abgefangen, d.h. bei Fehlern wird
 * so lange erneut zur Eingabe aufgefordert, bis ein g�ltiger Wert eingegeben
 * wird.<br />
 * Diese einfache Fehlerbehandlung kann ausgeschaltet werden, wenn man eine
 * eigene Fehlerbehandlung einbauen m�chte. In diesem Falle werden keine
 * Fehlermeldungen ausgegeben, die Fehlerpr�fung muss durch entsprechende
 * Abfragen selbst vorgenommen werden.
 * 
 * @see Tastatur#setEinfacheFehlerbehandlung
 *
 * @version 2.1 vom 20.09.2011
 * @author Ansorge, Griep bearbeitet von Francioni
 */
public final class Tastatur {
	/**
	 * Die Verbindung zur Tastatur
	 */
	static private BufferedReader b = null;
	/**
	 * Der letzte aufgetretene Fehler
	 */
	static private Exception fehler = null;
	/**
	 * Die einfache Fehlerbehandlung ist eingeschaltet.
	 */
	static private boolean einfacheFehlerbehandlung = true;

	/**
	 * "Ersatz" des Konstruktors. Wird vor jeder Eingabe aufgerufen. Stellt wenn
	 * n�tig eine Verbindung zur Tastatur her und setzt b. L�scht einen eventuell
	 * vorhandenen Fehler.
	 */
	static private void Tastatur() {
		if (b == null) {
			b = new BufferedReader(new InputStreamReader(System.in));
		}
		fehler = null;
	}

	/**
	 * Gibt die letzte Fehlermeldung als Exception zur�ck.
	 * 
	 * @return <b>Exception</b>, die die komplette Beschreibung des letzten Fehlers
	 *         enth�lt.<br />
	 *         <b>null</b>, wenn kein Fehler aufgetreten ist.
	 */
	static public Exception getFehler() {
		return fehler;
	}

	/**
	 * Gibt die letzte Fehlermeldung als Zeichenkette zur�ck.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * System.out.println(Tastatur.getFehlerMeldung());
	 * </pre>
	 * 
	 * <pre>
	 * String meinFehler = Tastatur.getFehlerMeldunge();
	 * </pre>
	 * 
	 * @return <b>Zeichenkette</b>, die die Beschreibung des letzten Fehlers
	 *         enth�lt.<br />
	 *         <i>leere Zeichenkette</i>, wenn kein Fehler aufgetreten ist.
	 */
	static public String getFehlerMeldung() {
		if (fehler != null)
			return fehler.toString();
		else
			return "";
	}

	/**
	 * Gibt zur�ck, ob bei der letzten Eingabe ein Fehler aufgetreten ist. Der
	 * Aufruf ist nur sinnvoll, wenn die einfache Fehlerbehandlung ausgeschaltet
	 * ist, da die Methoden im anderen Fall erst zur�ckkehren, wenn eine g�ltige
	 * Eingabe erfolgt ist - der Aufruf dieser Methode liefert bei eingeschalteter
	 * Fehlerbehandlung also immer <i>true</i>.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * if (Tastatur.istFehlerAufgetreten())
	 * 	System.out.println("Fehler");
	 * </pre>
	 * 
	 * @return <b>true</b>, wenn ein Fehler aufgetreten ist,<br />
	 *         <b>false</b>, wenn kein Fehler aufgetreten ist.
	 */
	static public boolean istFehlerAufgetreten() {
		return (fehler != null);
	}

	/**
	 * Setzt die Fehlerbehandlungsart. Wenn die einfache Fehlerbehandlung
	 * eingeschaltet ist, kann eine Eingabe nur durch einen korrekten Wert beendet
	 * werden. Bei Falscheingaben wird nach Ausgabe einer Fehlermeldung die Eingabe
	 * so lange erneut gefordert, bis ein g�ltiger Wert eingegeben wurde. <br />
	 * Ist die einfache Fehlerbehandlung ausgeschaltet, wird keine Fehlermeldung
	 * ausgegeben, sondern bei einem Fehler ein Standardwert zur�ckgegeben. �ber
	 * <i>getFehler</i> oder <i>getFehlerMeldung</i> kann der Fehler abgefragt und
	 * behandelt werden. Ein eventuell vorhandener Fehler wird zur�ckgesetzt.
	 * 
	 * @param art
	 *            <b>true</b> einfache Fehlerbehandlung ein,<br />
	 *            <b>false</b> einfache Fehlerbehandlung aus.
	 * @see Tastatur#getFehler
	 * @see Tastatur#getFehlerMeldung
	 */
	static public void setEinfacheFehlerbehandlung(boolean ein) {
		einfacheFehlerbehandlung = ein;
		fehler = null;
	}

	/**
	 * Gibt die aktuelle Fehlerbehandlungsart zur�ck.
	 * 
	 * @return <b>true</b> einfache Fehlerbehandlung, <br />
	 *         <b>false</b> eigene Fehlerbehandlung notwendig
	 */
	static public boolean istEinfacheFehlerbehandlung() {
		return einfacheFehlerbehandlung;
	}

	/**
	 * Liest eine mit Return abzuschlie�ende Zahl vom Typ <b>byte</b> von der
	 * Tastatur ein.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * byte b = Tastatur.liesByte();
	 * </pre>
	 * 
	 * @return die eingegebene byte-Zahl. <br />
	 * 		Im Fehlerfalle wird bei deaktivierter einfacher Fehlerbehandlung 0
	 *         zur�ckgegeben.
	 */
	static public byte liesByte() {
		do {
			try {
				return Byte.parseByte(liesString());
			} catch (Exception e) {
				fehler = e;
				if (!einfacheFehlerbehandlung)
					return 0;
				System.out.println("Dies ist kein gueltiger Byte-Wert. Bitte Eingabe wiederholen!");
			}
		} while (true);
	}

	/**
	 * Liest eine mit Return abzuschlie�ende Zahl vom Typ <b>short</b> von der
	 * Tastatur ein.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * short s = Tastatur.liesShort();
	 * </pre>
	 * 
	 * @return die eingegebene short-Zahl. <br />
	 * 		Im Fehlerfalle wird bei deaktivierter einfacher Fehlerbehandlung 0
	 *         zur�ckgegeben.
	 */
	static public short liesShort() {
		do {
			try {
				return Short.parseShort(liesString());
			} catch (Exception e) {
				fehler = e;
				if (!einfacheFehlerbehandlung)
					return 0;
				System.out.println("Dies ist kein gueltiger Short-Wert. Bitte Eingabe wiederholen!");
			}
		} while (true);
	}

	/**
	 * Liest eine mit Return abzuschlie�ende Zahl vom Typ <b>int</b> von der
	 * Tastatur ein.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * int i = Tastatur.liesInt();
	 * </pre>
	 * 
	 * @return die eingegebene int-Zahl. <br />
	 * 		Im Fehlerfalle wird bei deaktivierter einfacher Fehlerbehandlung 0
	 *         zur�ckgegeben.
	 */
	static public int liesInt() {
		do {
			try {
				return Integer.parseInt(liesString(), 10);
			} catch (Exception e) {
				fehler = e;
				if (!einfacheFehlerbehandlung)
					return 0;
				System.out.println("Dies ist kein gueltiger Integer-Wert. Bitte Eingabe wiederholen!");
			}
		} while (true);
	}

	/**
	 * Liest <b>das erste Zeichen</b> einer mit Return abzuschlie�enden Zeichenkette
	 * von der Tastatur.<br />
	 * Evtl. weitere eingegebenen Zeichen werden ignoriert.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * char c = Tastatur.liesChar();
	 * </pre>
	 * 
	 * @return das erste eingegebene Zeichen. <br />
	 * 		Bei deaktivierter einfacher Fehlerbehandlung wird im Fehlerfalle das
	 *         Zeichen Nr. 0 zur�ckgegeben.
	 */
	static public char liesChar() {
		do {
			try {
				return liesString().charAt(0);
			} catch (Exception e) {
				fehler = e;
				if (!einfacheFehlerbehandlung)
					return 0;
				System.out.println("Das ist kein gueltiges Zeichen. Bitte Eingabe wiederholen!");
			}
		} while (true);
	}

	/**
	 * Liest eine mit Return abzuschlie�ende Zahl vom Typ <b>long</b> von der
	 * Tastatur ein.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * long l = Tastatur.liesLong();
	 * </pre>
	 * 
	 * @return die eingegebene long-Zahl. <br />
	 * 		Im Fehlerfalle wird bei deaktivierter einfacher Fehlerbehandlung 0
	 *         zur�ckgegeben.
	 */
	static public long liesLong() {
		do {
			try {
				fehler = null;
				return Long.parseLong(liesString(), 10);
			} catch (Exception e) {
				fehler = e;
				if (!einfacheFehlerbehandlung)
					return 0;
				System.out.println("Dies ist kein gueltiger Long-Wert. Bitte Eingabe wiederholen!");
			}
		} while (true);
	}

	/**
	 * Liest einen mit Return abzuschlie�enden <b>boolean</b>-Wert von der
	 * Tastatur.<br />
	 * Die Methode liefert f�r alle Zeichenketten, die mit j, y, t oder 1 beginnen,
	 * den Wert <b>true</b>, f�r alle anderen <b>false</b>.<br />
	 * Gro�- und Kleinschreibung wird hierbei ignoriert.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * boolean b = Tastatur.liesBoolean();
	 * </pre>
	 * 
	 * @return der eingegebene boolean-Wert. <br />
	 * 		Im Fehlerfalle wird bei deaktivierter einfacher Fehlerbehandlung
	 *         <b>false</b> zur�ckgegeben.
	 */
	static public boolean liesBoolean() {
		do {
			try {
				fehler = null;
				String s = liesString();
				if (s.toUpperCase().charAt(0) == 'J' || s.toUpperCase().charAt(0) == 'Y'
						|| s.toUpperCase().charAt(0) == 'T' || s.toUpperCase().charAt(0) == '1')
					return true;
				if (s.toUpperCase().charAt(0) == 'N')
					return false;
				return Boolean.valueOf(s).booleanValue();
			} catch (Exception e) {
				fehler = e;
				if (!einfacheFehlerbehandlung)
					return false;
				System.out.println("Das ist kein gueltiger boolean-Wert. Bitte Eingabe wiederholen!");
			}
		} while (true);
	}

	/**
	 * Liest eine mit Return abzuschlie�ende Zahl vom Typ <b>double</b> von der
	 * Tastatur ein.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * double d = Tastatur.liesDouble();
	 * </pre>
	 * 
	 * @return die eingegebene double-Zahl. <br />
	 * 		Im Fehlerfalle wird bei deaktivierter einfacher Fehlerbehandlung 0.0
	 *         zur�ckgegeben.
	 */
	static public double liesDouble() {
		do {
			try {
				fehler = null;
				return Double.parseDouble(liesString());
			} catch (Exception e) {
				fehler = e;
				if (!einfacheFehlerbehandlung)
					return 0.0;
				System.out.println("Das ist kein gueltiger Double-Wert. Bitte Eingabe wiederholen!");
			}
		} while (true);
	}

	/**
	 * Liest eine mit Return abzuschlie�ende Zahl vom Typ <b>float</b> von der
	 * Tastatur ein.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * float f = Tastatur.liesFloat();
	 * </pre>
	 * 
	 * @return die eingegebene float-Zahl. <br />
	 * 		Im Fehlerfalle wird bei deaktivierter einfacher Fehlerbehandlung 0.0
	 *         zur�ckgegeben.
	 */
	static public float liesFloat() {
		do {
			try {
				fehler = null;
				return Float.parseFloat(liesString());
			} catch (Exception e) {
				fehler = e;
				if (!einfacheFehlerbehandlung)
					return 0;
				System.out.println("Das ist kein gueltiger float-Wert. Bitte Eingabe wiederholen!");
			}
		} while (true);
	}

	/**
	 * Liest eine mit Return abzuschlie�ende Zeichenkette von der Tastatur.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * String s = Tastatur.liesString();
	 * </pre>
	 * 
	 * @return die eingegebene Zeichenkette. <br />
	 * 		Im Fehlerfalle wird bei deaktivierter einfacher Fehlerbehandlung eine
	 *         leere Zeichenkette zur�ckgegeben.
	 */
	static public String liesString() {
		Tastatur();
		do {
			try {
				return b.readLine();
			} catch (Exception e) {
				fehler = e;
				if (!einfacheFehlerbehandlung)
					return "";
				System.out.println("Bei der Eingabe ist ein Fehler aufgetreten. Bitte wiederholen!");
			}
		} while (true);
	}

	/**
	 * Setzt die Fehlermeldung zur�ck. Nach dem Aufruf dieser Methode meldet
	 * <i>istFehlerAufgetreten</i> false.<br />
	 * Eine evtl. vorhandene Fehlermeldung kann nach dem Aufruf dieser Methode nicht
	 * mehr abgefragt werden.<br />
	 * Normalerweise setzen die Eingabemethoden den Fehler selbst zur�ck. Der Aufruf
	 * der Methode ist nur notwendig, wenn man eine eigene Fehlerbehandlung einbauen
	 * m�chte.<br />
	 * Beispiel:
	 * 
	 * <pre>
	 * Tastatur.loescheFehler();
	 * </pre>
	 * 
	 * @see Tastatur#istFehlerAufgetreten()
	 */
	static public void loescheFehler() {
		fehler = null;
	}
} // Tastatur

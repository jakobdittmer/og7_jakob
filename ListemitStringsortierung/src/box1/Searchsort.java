package box1;

import java.util.ArrayList;

public class Searchsort {

	public static ArrayList<String> bubblesort(ArrayList<String> zusortieren) {
		String temp;
		for (int i = 1; i < zusortieren.size(); i++) {
			for (int j = 0; j < zusortieren.size() - i; j++) {
				if(zusortieren.get(j).compareToIgnoreCase(zusortieren.get(j+1)) > 0) {
					temp = zusortieren.get(j);
					zusortieren.set(j, zusortieren.get(j+1));
					zusortieren.set(j+1, temp);
				}
			}
		}
		return zusortieren;
	}

	public static int linsuche(String tobesucht, ArrayList<String> tobedurchsucht) {
		return tobedurchsucht.indexOf(tobesucht);
	}

}

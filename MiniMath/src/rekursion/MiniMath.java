package rekursion;

public class MiniMath {

	public static int berechneZweiHoch(int i) {
		if (i > 31) {
			return -1;
		}
		if (i == 0) {
			return 1;
		}
		return 2 * berechneZweiHoch(i - 1);
	}

	public static int berechneSumme(int i) {
		if (i < 0) {
			return 0;
		}
		return i + berechneSumme(i - 1);
	}

	public static int berechneFakultaet(int i) {
		if (i <= 1) {
			return 1;
		}
		return i * berechneFakultaet(i - 1);
	}

}

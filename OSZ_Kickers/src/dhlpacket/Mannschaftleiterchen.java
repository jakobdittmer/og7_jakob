package dhlpacket;

public class Mannschaftleiterchen extends Spielerchen {
	
	private String mannschaftsname;
	private float rabatt;
	
	public String getMannschaftsname() {
		return mannschaftsname;
	}
	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}
	public float getRabatt() {
		return rabatt;
	}
	public void setRabatt(float rabatt) {
		this.rabatt = rabatt;
	}
	
	public Mannschaftleiterchen(String name, String vorname, int telefonnummer, boolean jahresbeitragbezahlt,
			int nummer, String position, String mannschaftsname, float rabatt) {
		super(name, vorname, telefonnummer, jahresbeitragbezahlt, nummer, position);
		this.mannschaftsname = mannschaftsname;
		this.rabatt = rabatt;
	}
}

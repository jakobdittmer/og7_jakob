package dhlpacket;

public abstract class Personchen {
	
	protected String name;
	protected String vorname;
	protected int telefonnummer;
	protected boolean jahresbeitragbezahlt;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public int getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(int telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public boolean isJahresbeitragbezahlt() {
		return jahresbeitragbezahlt;
	}
	public void setJahresbeitragbezahlt(boolean jahresbeitragbezahlt) {
		this.jahresbeitragbezahlt = jahresbeitragbezahlt;
	}
	
	public Personchen(String name, String vorname, int telefonnummer, boolean jahresbeitragbezahlt) {
		this.name = name;
		this.vorname = vorname;
		this.telefonnummer = telefonnummer;
		this.jahresbeitragbezahlt = jahresbeitragbezahlt;
	}
	

	
	
}
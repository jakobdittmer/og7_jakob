package dhlpacket;

public class Refchen extends Personchen {
	
	private int anzSpiele;

	public int getAnzSpiel() {
		return anzSpiele;
	}

	public void setAnzSpiel(int anzSpiel) {
		this.anzSpiele = anzSpiel;
	}

	public Refchen(String name, String vorname, int telefonnummer, boolean jahresbeitragbezahlt, int anzSpiele) {
		super(name, vorname, telefonnummer, jahresbeitragbezahlt);
		this.anzSpiele = anzSpiele;
	}

}

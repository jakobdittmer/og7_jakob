package dhlpacket;

public class Spielerchen extends Personchen {

	protected int nummer;
	protected String position;

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Spielerchen(String name, String vorname, int telefonnummer, boolean jahresbeitragbezahlt, int nummer, String position) {
		super(name, vorname, telefonnummer, jahresbeitragbezahlt);
		this.nummer = nummer;
		this.position = position;
	}


}

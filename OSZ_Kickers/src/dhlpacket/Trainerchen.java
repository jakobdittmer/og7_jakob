package dhlpacket;

public class Trainerchen extends Personchen {
	
	private char lizensztyp;
	private double aufwansentschaedigung;
	
	public char getLiensztyp() {
		return lizensztyp;
	}

	public void setLiensztyp(char liensztyp) {
		this.lizensztyp = liensztyp;
	}

	public double getAufwansentschaedigung() {
		return aufwansentschaedigung;
	}

	public void setAufwansentschaedigung(double aufwansentschaedigung) {
		this.aufwansentschaedigung = aufwansentschaedigung;
	}

	public Trainerchen(String name, String vorname, int telefonnummer, boolean jahresbeitragbezahlt, char lizensztyp, double aufwansentschaedigung) {
		super(name, vorname, telefonnummer, jahresbeitragbezahlt);
		this.lizensztyp = lizensztyp;
		this.aufwansentschaedigung = aufwansentschaedigung;
	}
	
	
}

package karte;

public class Held {
	String name;
	String typus;
	String beschreibung;
	double maxHP;
	double currentHP;
	double armor;
	double magicresistence;
	double ad;
	double atackspeed;
	String picture;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypus() {
		return typus;
	}

	public void setTypus(String typus) {
		this.typus = typus;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public double getMaxHP() {
		return maxHP;
	}

	public void setMaxHP(double maxHP) {
		this.maxHP = maxHP;
	}

	public double getCurrentHP() {
		return currentHP;
	}

	public void setCurrentHP(double currentHP) {
		this.currentHP = currentHP;
	}

	public double getArmor() {
		return armor;
	}

	public void setArmor(double armor) {
		this.armor = armor;
	}

	public double getMagicresistence() {
		return magicresistence;
	}

	public void setMagicresistence(double magicresistence) {
		this.magicresistence = magicresistence;
	}

	public double getAd() {
		return ad;
	}

	public void setAd(double ad) {
		this.ad = ad;
	}

	public double getAtackspeed() {
		return atackspeed;
	}

	public void setAtackspeed(double atackspeed) {
		this.atackspeed = atackspeed;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Held() {
		super();
	}

	public Held(String name, String typus, String beschreibung, double maxHP, double ad, double armor,
			double magicresistence) {
		super();
		this.name = name;
		this.typus = typus;
		this.beschreibung = beschreibung;
		this.maxHP = maxHP;
		this.currentHP = maxHP;
		this.ad = ad;
		this.armor = armor;
		this.magicresistence = magicresistence;
	}

	@Override
	public String toString() {
		return "Held [name=" + name + ", typus=" + typus + ", beschreibung=" + beschreibung + ", maxHP=" + maxHP
				+ ", currentHP=" + currentHP + ", armor=" + armor + ", magicresistence=" + magicresistence + ", ad="
				+ ad + ", atackspeed=" + atackspeed + ", picture=" + picture + "]";
	}

	public double angreifen() {
		return this.ad;
	}
	
	public void leiden(double dmg) {
		if(this.armor >= 0) {
			this.currentHP -= (100/(100+this.armor))*dmg ;
		}
		else {
			this.currentHP -= (2-(100/(100-this.armor))*dmg);
		}
	}
	
	public void heilen() {
		//+ spiritvisage?
		this.currentHP = this.maxHP;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

package karte;

import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JSplitPane;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.Component;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.UIManager;
import java.awt.Dimension;
import javax.swing.BoxLayout;

public class Karte extends JPanel {
	public Held getHeld() {
		return held;
	}

	public void setHeld(Held held) {
		this.held = held;
	}

	Held held;

	public Karte(Held held) {
		this.held = held;
		setBackground(Color.LIGHT_GRAY);
		setLayout(new BorderLayout(0, 0));
		
		JPanel headerpane = new JPanel();
		headerpane.setBackground(Color.GRAY);
		add(headerpane, BorderLayout.NORTH);
		headerpane.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel namebox = new JLabel(held.getName());
		namebox.setVerticalAlignment(SwingConstants.TOP);
		namebox.setForeground(Color.YELLOW);
		namebox.setBackground(Color.WHITE);
		namebox.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 25));
		namebox.setHorizontalAlignment(SwingConstants.LEFT);
		headerpane.add(namebox);
		
		JLabel typbox = new JLabel(held.getTypus());
		typbox.setVerticalAlignment(SwingConstants.BOTTOM);
		typbox.setForeground(Color.YELLOW);
		typbox.setBackground(Color.WHITE);
		typbox.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 25));
		typbox.setHorizontalAlignment(SwingConstants.RIGHT);
		headerpane.add(typbox);
		
		JPanel middle = new JPanel();
		add(middle, BorderLayout.CENTER);
		middle.setLayout(new BorderLayout(0, 0));
		
		JLabel picture = new JLabel("");
		middle.add(picture, BorderLayout.NORTH);
		picture.setBorder(UIManager.getBorder("Button.border"));
		picture.setIcon(new ImageIcon(new ImageIcon(held.getPicture()).getImage().getScaledInstance(254, 320, Image.SCALE_SMOOTH)));
		//picture.setIcon(new ImageIcon(held.getPicture()));
		
		JLabel desc = new JLabel(held.getBeschreibung());
		desc.setVerticalAlignment(SwingConstants.BOTTOM);
		middle.add(desc, BorderLayout.SOUTH);
		desc.setFont(new Font("Sitka Text", Font.BOLD, 15));
		desc.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel statspane = new JPanel();
		statspane.setBackground(Color.DARK_GRAY);
		add(statspane, BorderLayout.SOUTH);
		statspane.setLayout(new GridLayout(4, 2, 0, 0));
		
		JLabel AD = new JLabel("Atack Damage");
		AD.setForeground(new Color(255, 0, 0));
		AD.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		statspane.add(AD);
		
		JLabel advalue = new JLabel(String.valueOf(held.getAd()));
		advalue.setForeground(new Color(255, 0, 0));
		advalue.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		advalue.setHorizontalAlignment(SwingConstants.CENTER);
		statspane.add(advalue);
		
		JLabel Armor = new JLabel("Armor");
		Armor.setForeground(Color.ORANGE);
		Armor.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		statspane.add(Armor);
		
		JLabel armorvalue = new JLabel(String.valueOf(held.getArmor()));
		armorvalue.setForeground(Color.ORANGE);
		armorvalue.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		armorvalue.setHorizontalAlignment(SwingConstants.CENTER);
		statspane.add(armorvalue);
		
		JLabel mr = new JLabel("Magicresistence");
		mr.setForeground(Color.CYAN);
		mr.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		statspane.add(mr);
		
		JLabel mrvalue = new JLabel(String.valueOf(held.getMagicresistence()));
		mrvalue.setForeground(Color.CYAN);
		mrvalue.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		mrvalue.setHorizontalAlignment(SwingConstants.CENTER);
		statspane.add(mrvalue);
		
		JLabel HP = new JLabel("Base Hitpoints");
		HP.setForeground(Color.GREEN);
		HP.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		HP.setHorizontalAlignment(SwingConstants.LEFT);
		statspane.add(HP);
		
		JLabel hpvalue = new JLabel(String.valueOf(held.getMaxHP()));
		hpvalue.setForeground(Color.GREEN);
		hpvalue.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		hpvalue.setHorizontalAlignment(SwingConstants.CENTER);
		statspane.add(hpvalue);
	}
}

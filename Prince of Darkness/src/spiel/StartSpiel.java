package spiel;

import karte.Held;

/**
  *
  * Klasse f�r das Starten des Spiels
  *
  * @version 1.0 vom 16.10.2012
  * @author Tenbusch
  */

public class StartSpiel {

  public static void main(String[] args) {
    Held lulu = new Held("Lulu", "Yordel", "The Fae Sorceress", 120, 60, 1, 25);
    Held sona = new Held("FULL AP SONA", "Enchanter", "Demacian Artist", 60, 10*Math.PI, 100, 16);
    sona.setPicture("src/bilder/sona.PNG");
    lulu.setPicture("src/bilder/lulu.PNG");
    new PrinceOfDarkness("Prince of Darkness", sona, lulu);
  }
}

/**
  *	Ich will mal anmerken das es ein paar Probleme mit dem Spiel gibt:
  *	
  *Der rechte Lebensbalken ist verbuggt, enfach mal resetten dann siehste es.
  *
  *Da ist ein balancing Problem mit Armor, Charaktere mit hoher Armor z.B. durch Buffs oder andere F�higkeiten
  *erreichen einen Powerspike und zeitgleich ein Hardcap, wenn ihr Armor h�her ist als die AD des Gegners, denn sie nehmen
  *ob dort an keinen Schaden mehr, ich empfehle hierf�r die Schadensreduzierung prozentual zu regeln und vieleicht mit einem Softcap zu arbeiten.
  *
  *Dazu empfehle ich zur Schadensberechnung eine Klasse "Schaden" einzuf�gen, welche zwei Attribute: MagicDMG und PyhsicDMG einbezieht um
  *mit MR und Armor zu arbeiten und andere schadensbeintr�chtigende Effekte und Buffs zu realisieren.
  *
  *Etwas mehr interaktives Gameplay sollte auch im Vordergrund stehen, Aktionen welche man ausw�hlen kann bevor man seinen Zug beendet w�re nice und
  *dazu eine AI welche das selbe macht.
  *
  *Mein Background geht nicht!
  */
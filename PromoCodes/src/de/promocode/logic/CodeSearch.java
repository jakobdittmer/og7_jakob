package de.promocode.logic;

public class CodeSearch {

	public static int NOT_FOUND = -1;

	/**
	 * Sucht einen Code in der Liste.
	 * 
	 * @param list
	 *            Liste der Codes im Long-Format
	 * @param n
	 *            Gesuchter Code
	 * @return Index des Codes, wenn er vorhanden ist, NOT_FOUND wenn er nicht
	 *         gefunden werden konnte
	 */
	public static int findPromoCode(long[] list, long n, int start, int ende) { //I guess ich musste zur PromoCodeHandler.java und dort den Methodenaufruf wegen den neuen Parametern überarbeiten o.O
		// Schaut ob es möglich ist, dass die Zahl in der Liste enthalten ist.
		if (n > list[(list.length) - 1]) {
			return NOT_FOUND;
		}

		int mitte = (start + ende) / 2;
		while (start <= ende) {
			// Schaut ob in der Mitte n ist.
			if (n == list[mitte]) {
				return mitte;
			}

			if (n < list[mitte]) {
				return findPromoCode(n, list, start, (mitte - 1));
			}
			if (n > list[mitte]) {
				return findPromoCode(n, list, (mitte + 1), ende);
			}
		}
		return NOT_FOUND;
	}

}

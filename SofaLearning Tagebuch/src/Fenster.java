import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.table.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JScrollPane;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.border.BevelBorder;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.awt.event.ActionEvent;
import javax.swing.JInternalFrame;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.SwingConstants;
import javax.swing.JRadioButtonMenuItem;

public class Fenster extends JFrame {

	private JPanel contentPane;
	private Logic logic;
	final JFileChooser fc = new JFileChooser();
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Fenster frame = new Fenster();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Fenster() {
		setResizable(false);
		setTitle("SofaLearning Tagebuch");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnDatei = new JMenu("Datei");
		menuBar.add(mnDatei);

		JMenuItem mntmImportiereTagebuch = new JMenuItem("Importiere Tagebuch");
		mntmImportiereTagebuch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int returnVal = fc.showOpenDialog(contentPane);
			}
		});
		mnDatei.add(mntmImportiereTagebuch);

		JMenuItem mntmExportiereTagebuch = new JMenuItem("Exportiere Tagebuch");
		mntmExportiereTagebuch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = fc.showOpenDialog(contentPane);

			}
		});
		mnDatei.add(mntmExportiereTagebuch);

		JMenuItem mntmErstelleTagebuch = new JMenuItem("Erstelle Tagebuch");
		mntmErstelleTagebuch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				/*
				 * Ich wusste nicht ich nen neues leeres TableModel auf die Tabelle setzte, 
				 * also muss das hier gehen, laufzeittechnisch ne Katastrophe aber wer lernt schon so viel bruh :D
				 */
				for(int i = model.getRowCount();i>0;i--) {
					model.removeRow(i-1);
				}
				logic.creatTagebuch();
			}
		});
		mnDatei.add(mntmErstelleTagebuch);

		JMenu mnBearbeiten = new JMenu("Bearbeiten");
		menuBar.add(mnBearbeiten);

		JMenuItem mntmErstelleEintrag = new JMenuItem("Erstelle Eintrag");
		mntmErstelleEintrag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.addRow(new Object[] {}); // f�gt ne Zeile hinzu, auch wenn ich keinen Plan habe wie das geht aber ok
			}
		});
		mnBearbeiten.add(mntmErstelleEintrag);

		JMenuItem mntmLscheEintrag = new JMenuItem("L\u00F6sche Eintrag");
		mntmLscheEintrag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				if (model.getRowCount() > 0) {	//Nur um Fehler in der Konsole zu vermeiden falls wer eine row l�schen will wenn es keine mehr gibt
					model.removeRow(model.getRowCount() - 1); // Zeilen fangen bei 0 an
				}
			}
		});
		mnBearbeiten.add(mntmLscheEintrag);
		
		JMenu mnHilfe = new JMenu("Hilfe");
		menuBar.add(mnHilfe);
		
		JCheckBoxMenuItem chckbxmntmIchBraucheHilfe = new JCheckBoxMenuItem("Ich brauche hilfe");
		mnHilfe.add(chckbxmntmIchBraucheHilfe);
		
		JCheckBoxMenuItem chckbxmntmIchWillHilfe = new JCheckBoxMenuItem("Ich will hilfe");
		mnHilfe.add(chckbxmntmIchWillHilfe);
		
		JCheckBoxMenuItem chckbxmntmMirIstNicht = new JCheckBoxMenuItem("Mir ist nicht zu helfen");
		chckbxmntmMirIstNicht.setSelected(true);
		mnHilfe.add(chckbxmntmMirIstNicht);
		
		JCheckBoxMenuItem chckbxmntmIchHabeRealisiert = new JCheckBoxMenuItem("Ich habe realisiert das mich dieses Men\u00FC nicht weiter bringt");
		mnHilfe.add(chckbxmntmIchHabeRealisiert);

		Component abstandhaltermenubar = Box.createHorizontalStrut(280);
		menuBar.add(abstandhaltermenubar);
		contentPane = new JPanel();
		contentPane.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Datum", "Fach", "Aktivit\u00E4t", "verschwendete Zeit"
			}
		));
		table.getColumnModel().getColumn(3).setPreferredWidth(118);
		scrollPane.setViewportView(table);

	}

}

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

public class Logic {

	public Logic() {
	}

	/*
	 * Gerade Eingelesene Liste
	 */
	public Tagebuch datei;
	// public Logic(FileControl);
	// public void reloadFromDisk();
	// public List<Lerneintrag> getListEntries();

	public void readDatei(String ziel) {
		
	}

	public void saveDatei(String ziel) {
		
	}

	public void creatTagebuch() {
		String now = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date());
		String zeit = now.toString(); // String in toString...wat
		String username = System.getProperty("user.name");
		// String userName = new com.sun.security.auth.module.NTSystem().getName(); does not work :/
		this.datei = new Tagebuch(username, zeit, "empty", new DefaultTableModel());
	}

	public String getLearnerName() {
		return datei.getUser();
	}

	public Tagebuch getDatei() {
		return datei;
	}

	public void setDatei(Tagebuch datei) {
		this.datei = datei;
	}

}

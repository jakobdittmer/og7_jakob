import javax.swing.table.TableModel;
public class Tagebuch {
	private String user;
	private String erstellungsdatum;
	private TableModel data;

	public Tagebuch() {
		super();
	}

	public Tagebuch(String user, String erstellungsdatum, String kommentar, TableModel data) {
		super();
		this.user = user;
		this.erstellungsdatum = erstellungsdatum;
		this.data = data;

	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getErstellungsdatum() {
		return erstellungsdatum;
	}

	public void setErstellungsdatum(String erstellungsdatum) {
		this.erstellungsdatum = erstellungsdatum;
	}

	public TableModel getData() {
		return data;
	}

	public void setData(TableModel data) {
		this.data = data;
	}
}

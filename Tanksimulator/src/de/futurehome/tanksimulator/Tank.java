package de.futurehome.tanksimulator;

public class Tank {

	private double fuellstand;
	private double fuellstandcap;

	public double getFuellstandcap() {
		return fuellstandcap;
	}

	public void setFuellstandcap(double fuellstandcap) {
		this.fuellstandcap = fuellstandcap;
	}

	public Tank(double fuellstand, double fuellstandcap) {
		this.fuellstand = fuellstand;
		this.fuellstandcap = fuellstandcap;
	}

	public double getFuellstand() {
		return fuellstand;
	}

	public void setFuellstand(double fuellstand) {
		this.fuellstand = fuellstand;
	}

}

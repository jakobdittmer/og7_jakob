package zooTycoon;

public class Addon {
	private String bezeichnung;
	private int id;
	private double preis;
	private int bestand;
	private int maxBestand;

	public Addon(String bezeichnung, int id, double preis, int bestand, int maxBestand) {
		super();
		this.bezeichnung = bezeichnung;
		this.id = id;
		this.preis = preis;
		this.bestand = bestand;
		this.maxBestand = maxBestand;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}

	public int getMaxBestand() {
		return maxBestand;
	}

	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}

	public void kaufen(int menge) {
		if ((this.getBestand() + menge) > this.getMaxBestand()) {
			this.setBestand(this.getBestand());
		} else {
			this.bestand = this.bestand + menge;
		}
	}

	public void verbrauchen(int menge) {
		if ((this.getBestand() - menge) < 0) {
			this.setBestand(0);
		} else {
			this.bestand = this.bestand - menge;
		}
	}
}

package box1;

public class Binaeresuche {
	public static boolean istZahlenthalten(long n, long[] zahlenliste, int start, int ende) {

		// Schaut ob es m�glich ist, dass die Zahl in der Liste enthalten ist.
		if (n > zahlenliste[(zahlenliste.length) - 1]) {
			return false;
		}

		int mitte = (start + ende) / 2;
		while (start <= ende) {
			// Schaut ob in der Mitte n ist.
			if (n == zahlenliste[mitte]) {
				return true;
			}

			if (n < zahlenliste[mitte]) {
				return istZahlenthalten(n, zahlenliste, start, (mitte - 1));
			}
			if (n > zahlenliste[mitte]) {
				return istZahlenthalten(n, zahlenliste, (mitte + 1), ende);
			}
		}
		return false;

	}

}

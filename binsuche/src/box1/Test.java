package box1;

public class Test {

	public static void main(String[] args) {

		long[] zahlenreihe = new long[15000000];
		zahlenreihe = SortedListGenerator.getSortedList(15000000);
		Stopuhr s1 = new Stopuhr();
		while (true) {
			System.out.println(zahlenreihe[0] + " & " + zahlenreihe[zahlenreihe.length - 1] + " :worstcase");
			System.out.println(zahlenreihe[zahlenreihe.length / 2] + " :bestcase");
			
			System.out.println("Zahleingeben...");
			long z = Tastatur.liesLong();
			s1.start();
			System.out.println(Binaeresuche.istZahlenthalten(z, zahlenreihe, 0, zahlenreihe.length));
			s1.stop();
			System.out.println(s1.getZeit());
			s1.reset();
		}
	}
}
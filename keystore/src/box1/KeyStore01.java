package box1;

public class KeyStore01 {

	private int arrayf�llstand;
	private String[] schl�sselliste;

	public KeyStore01() {
		schl�sselliste = new String[100];
		arrayf�llstand = 0;
	}

	/**
	 * 
	 * @param l�nge L�nge des Arrays
	 */
	public KeyStore01(int l�nge) {
		schl�sselliste = new String[l�nge];
		arrayf�llstand = 0;
	}

	/**
	 * 
	 * @param i index des Elements
	 * @return Element mit index i
	 */
	public String get(int i) {
		return schl�sselliste[i];
	}

	/**
	 * 
	 * @return Arrayf�llstand
	 */
	public int size() {
		// return schl�sselliste.length;
		return arrayf�llstand;
	}

	/**
	 * Bullshit
	 */
	public void clear() {
		arrayf�llstand = 0; // das reicht eig, aber die Elemente im Array sind noch vorhanden und k�nnen abgerufen werden o.O
	}

	/**
	 * 
	 * @param hinzugef�gt der zuhinzuzuf�gende String ._.
	 * @return	true oder nicht true, das ist hier die Frage...
	 */
	public boolean add(String hinzugef�gt) {
		if (arrayf�llstand < schl�sselliste.length) {
			schl�sselliste[arrayf�llstand] = hinzugef�gt;
			arrayf�llstand++;
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param i index des zu l�schenden Elements
	 * @return	true = Element wurde gel�scht und f�llstand wurde um 1 zur�ckgesetzt / false = das Array ist nicht weit genug gef�llt(billige Array out of bound exception)
	 */
	public boolean remove(int i) {
		if ((i >= 0) && (i < arrayf�llstand)) {
			for (int x = i; x < schl�sselliste.length - 1; x++) {	//im grunde kann man das auch in die andere remove Methode machen
				schl�sselliste[x] = schl�sselliste[x + 1];
			}
			arrayf�llstand--;
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param wort der zu entfernenende String
	 * @return gibt zur�ck ob das ELement gel�scht wird, und nicht :3
	 */
	public boolean remove(String wort) {
		if (indexOf(wort) == -1)
			return false;
		return remove(indexOf(wort));
	}

	/**
	 * 
	 * @param wort
	 *            Element im Array von dem der Index gebraucht wird.
	 * @return Index des ersten Auftreten im Arrays
	 */
	public int indexOf(String wort) {
		int i = 0;
		while ((i < arrayf�llstand) && (!schl�sselliste[i].equals(wort))) {
			i++;
		}
		if (i >= arrayf�llstand) {
			return (-1);
		}
		return (i);
	}

	public String toString() {
		String ausgabe = "";
		for (int i = 0; i < arrayf�llstand; i++)
			ausgabe = ausgabe + schl�sselliste[i] + " <+> ";
		return ausgabe;
	}

}
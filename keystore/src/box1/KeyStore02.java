package box1;

import java.util.ArrayList;

public class KeyStore02 {

	private ArrayList<String> schlüsselliste;

	public KeyStore02() {
		schlüsselliste = new ArrayList<String>();

	}

	/**
	 * 
	 * @param länge
	 *            Länge des Arrays
	 */
	public KeyStore02(int länge) {
		schlüsselliste = new ArrayList<String>(länge);

	}

	/**
	 * 
	 * @param i
	 *            index des Elements
	 * @return Element mit index i
	 */
	public String get(int i) {
		return schlüsselliste.get(i);
	}

	/**
	 * 
	 * @return Arrayfüllstand
	 */
	public int size() {
		return schlüsselliste.size();
	}

	/**
	 * Bullshit
	 */
	public void clear() {
		schlüsselliste.clear();
	}

	/**
	 * 
	 * @param hinzugefügt
	 *            der zuhinzuzufügende String ._.
	 * @return true oder nicht true, das ist hier die Frage...
	 */
	public boolean add(String hinzugefügt) {
		schlüsselliste.add(hinzugefügt);
		return true;
	}

	/**
	 * 
	 * @param wort
	 *            der zu entfernenende String
	 * @return gibt zurück ob das ELement gelöscht wird, und nicht :3
	 */
	public boolean remove(String wort) {
		if (schlüsselliste.contains(wort)) {
			schlüsselliste.remove(wort);
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param wort
	 *            Element im Array von dem der Index gebraucht wird.
	 * @return Index des ersten Auftreten im Arrays
	 */
	public int indexOf(String wort) {
		return schlüsselliste.indexOf(wort);
	}

	public String toString() {
		return schlüsselliste.toString();
	}
}

package box1;

public class Haustier {

	private final byte max = 100;
	private final byte min = 0;
	private int hunger = max;
	private int muede = max;
	private int happy = max;
	private int healthy = max;
	private String name = "Nicht definierter Name!";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Haustier(int hunger, int muede, int happy, int health, String name) {
		super();
		this.hunger = hunger;
		this.muede = muede;
		this.happy = happy;
		this.healthy = health;
		this.name = name;
	}

	public Haustier(String name) {
		super();
		this.name = name;
	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int newhunger) {

		if (newhunger > max) {
			newhunger = max;
		}
		if (newhunger < min) {
			newhunger = min;
		}
		this.hunger = newhunger;
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int newmuede) {

		if (newmuede > max) {
			newmuede = max;
		}
		if (newmuede < min) {
			newmuede = min;
		}
		this.muede = newmuede;
	}

	public int getZufrieden() {
		return happy;
	}

	public void setZufrieden(int zufrieden) {
		if (zufrieden > max) {
			zufrieden = max;
		}
		if (zufrieden < min) {
			zufrieden = min;
		}
		this.happy = zufrieden;
	}

	public int getGesund() {
		return healthy;
	}

	public void setGesund(int newhealth) {
		if (newhealth > max) {
			newhealth = max;
		}
		if (newhealth < min) {
			newhealth = min;
		}
		this.healthy = newhealth;
	}

	public void fuettern(int i) {
		setHunger(getHunger() + i);
	}

	public void healing() {
		setGesund(max);
	}

	public void sleep(int i) {
		setMuede(getMuede() + i);
	}

	public void spielen(int i) {
		setZufrieden(getZufrieden() + i);
	}

}

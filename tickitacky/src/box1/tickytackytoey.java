package box1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;
import javax.swing.border.BevelBorder;

public class tickytackytoey extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					tickytackytoey frame = new tickytackytoey();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public tickytackytoey() {
		setVisible(true);
		setBackground(new Color(0, 0, 0));
		setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		setTitle("ff @ 20");
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(3, 3, 0, 0));

		for (int i = 0; i < 9; i++) {
			JButton btnNewButton = new JButton("");
			btnNewButton.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			btnNewButton.setForeground(new Color(0, 255, 255));
			btnNewButton.setBackground(new Color(138, 43, 226));
			btnNewButton.setFont(new Font("Comic Sans MS", Font.BOLD, 32));
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					tickytackytoey.changeZUSTAND(btnNewButton);
				}
			});
			contentPane.add(btnNewButton);

		}
	}

	public static void changeZUSTAND(JButton b) {
		if (b.getText().equals("")) {
			b.setText("X");
		} else if (b.getText().equals("X")) {
			b.setText("O");
		} else {
			b.setText("");
		}
	}

}

package testlayouts;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import javax.swing.JSpinner;
import javax.swing.JEditorPane;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JProgressBar;

public class test extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					test frame = new test();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public test() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel flow = new JPanel();
		tabbedPane.addTab("flow", null, flow, null);
		flow.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnNewButton = new JButton("FF @ 20");
		flow.add(btnNewButton);
		
		JButton btnNewButton_4 = new JButton("MID OR INT");
		flow.add(btnNewButton_4);
		
		JButton btnNewButton_1 = new JButton("LOL Tennyboy");
		flow.add(btnNewButton_1);
		
		JButton btnNewButton_3 = new JButton("i hate YASUO");
		flow.add(btnNewButton_3);
		
		JButton btnNewButton_6 = new JButton("\"R\"");
		flow.add(btnNewButton_6);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("box", null, panel, null);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		JButton btnNewButton_7 = new JButton("New buttonzhrshs");
		btnNewButton_7.setAlignmentY(CENTER_ALIGNMENT);
		panel.add(btnNewButton_7);
		
		JButton btnNewButton_12 = new JButton("New");
		btnNewButton_12.setAlignmentY(BOTTOM_ALIGNMENT);
		panel.add(btnNewButton_12);
		
		JButton btnNewButton_8 = new JButton("New button");
		btnNewButton_8.setAlignmentY(TOP_ALIGNMENT);
		panel.add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("New button");
		panel.add(btnNewButton_9);
		
		JButton btnNewButton_10 = new JButton("New button");
		panel.add(btnNewButton_10);
		
		JButton btnNewButton_11 = new JButton("New button");
		panel.add(btnNewButton_11);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("border", null, panel_1, null);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JButton btnNewButton_2 = new JButton("New button");
		panel_1.add(btnNewButton_2, BorderLayout.WEST);
		
		JSpinner spinner = new JSpinner();
		panel_1.add(spinner, BorderLayout.EAST);
		
		JButton btnNewButton_5 = new JButton("New button");
		panel_1.add(btnNewButton_5, BorderLayout.NORTH);
		
		JToggleButton tglbtnNewToggleButton = new JToggleButton("New toggle button");
		panel_1.add(tglbtnNewToggleButton, BorderLayout.CENTER);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("New radio button");
		panel_1.add(rdbtnNewRadioButton, BorderLayout.SOUTH);
		
		
	}

}
